﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace textifant.Models
{
    public class Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] /*Auto Increment */
        public int fld_accountId { get; set; }

        /*Foreign key */
        [ForeignKey("fld_roleidref")]
        [Required]
        public virtual Role fld_role { get; set; }
        public int fld_roleidref { get; set; }
        /* **** */ 

        [Required]
        public string fld_lastName { get; set; }
        [Required]
        public string fdl_firstname { get; set; }
        [Required]
        public string fld_email{ get; set; }
        [Required]
        public string fld_password { get; set; }
        [Required]
        public DateTime fld_creadate { get; set; }
        public DateTime fld_obsdate { get; set; }
        public Boolean fld_verified { get; set; }
        public Boolean fld_active { get; set; }
        public String fld_street { get; set; }
        public String fld_zip { get; set; }
        public String fld_city { get; set; }
        public String fld_country { get; set; }
        public int fld_tel { get; set; }
        public int fld_fax { get; set; }
        public String fld_notes { get; set; }



    }
}