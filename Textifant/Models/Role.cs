﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace textifant.Models
{
    public class Role
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int fld_roleid { get; set; }

        [ForeignKey("fld_roleidref")]
        [Required]
        public ICollection<Account> col_accounts { get; set; }

        [Required]
        public string fld_description { get; set; }

    
    }
}